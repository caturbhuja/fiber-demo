package permission

import (
	"github.com/crazyhl/gopermission/models"
	"github.com/gofiber/fiber/v2"
	"go-fiber-demo/pkg"
	"go-fiber-demo/pkg/db"
	"gorm.io/gorm/clause"
	"strconv"
)

// 添加权限
func Add(ctx *fiber.Ctx) error {
	permission := new(models.Permission)
	permission.Name = ctx.FormValue("name", "")
	permission.Description = ctx.FormValue("description", "")
	permission.Url = ctx.FormValue("url", "")
	permission.ModelName = ctx.FormValue("modelName", "")
	permission.UrlParamName = ctx.FormValue("urlParamName", "")
	permission.ModelCheckCondition = ctx.FormValue("modelCheckCondition", "")

	response := new(pkg.Response)

	if permission.ModelName != "" && (permission.UrlParamName == "" || permission.ModelCheckCondition == "") {
		response.Status = -3
		response.Message = "Url, ModelName, UrlParamName, ModelCheckCondition 中有一项填写则此四项比如全部填写"

		return ctx.JSON(response)
	}

	if permission.UrlParamName != "" && (permission.ModelName == "" || permission.ModelCheckCondition == "") {
		response.Status = -3
		response.Message = "Url, ModelName, UrlParamName, ModelCheckCondition 中有一项填写则此四项比如全部填写"

		return ctx.JSON(response)
	}

	if permission.ModelCheckCondition != "" && (permission.ModelName == "" || permission.UrlParamName == "") {
		response.Status = -3
		response.Message = "Url, ModelName, UrlParamName, ModelCheckCondition 中有一项填写则此四项比如全部填写"

		return ctx.JSON(response)
	}

	err := permission.Add()

	if err != nil {
		if err.Error()[:db.ErrorDuplicateStrLength] == db.ErrorDuplicate {
			response.Status = -2
			response.Message = "已存在重复数据"
		} else {
			response.Status = -1
			response.Message = err.Error()
		}

		return ctx.JSON(response)
	}

	return ctx.JSON(permission)
}

// 更新权限
func Update(ctx *fiber.Ctx) error {
	ruleId := ctx.Params("id", "")
	gormDb := pkg.GetConfig().GormDb
	permission := new(models.Permission)

	gormDb.Where("id = ?", ruleId).First(&permission)
	response := new(pkg.Response)
	if permission.ID == 0 {
		response.Status = -1
		response.Message = "权限不存在"
		return ctx.JSON(response)
	}

	permission.Name = ctx.FormValue("name", "")
	permission.Description = ctx.FormValue("description", "")
	permission.Url = ctx.FormValue("url", "")
	permission.ModelName = ctx.FormValue("modelName", "")
	permission.UrlParamName = ctx.FormValue("urlParamName", "")
	permission.ModelCheckCondition = ctx.FormValue("modelCheckCondition", "")

	if permission.ModelName != "" && (permission.UrlParamName == "" || permission.ModelCheckCondition == "") {
		response.Status = -3
		response.Message = "Url, ModelName, UrlParamName, ModelCheckCondition 中有一项填写则此四项比如全部填写"

		return ctx.JSON(response)
	}

	if permission.UrlParamName != "" && (permission.ModelName == "" || permission.ModelCheckCondition == "") {
		response.Status = -3
		response.Message = "Url, ModelName, UrlParamName, ModelCheckCondition 中有一项填写则此四项比如全部填写"

		return ctx.JSON(response)
	}

	if permission.ModelCheckCondition != "" && (permission.ModelName == "" || permission.UrlParamName == "") {
		response.Status = -3
		response.Message = "Url, ModelName, UrlParamName, ModelCheckCondition 中有一项填写则此四项比如全部填写"

		return ctx.JSON(response)
	}

	err := permission.Update()

	if err != nil {
		if err.Error()[:db.ErrorDuplicateStrLength] == db.ErrorDuplicate {
			response.Status = -2
			response.Message = "已存在重复数据"
		} else {
			response.Status = -1
			response.Message = err.Error()
		}

		return ctx.JSON(response)
	}

	return ctx.JSON(permission)
}

// 删除权限
func Delete(ctx *fiber.Ctx) error {
	permissionId := ctx.Params("id", "")
	gormDb := pkg.GetConfig().GormDb
	permission := new(models.Permission)

	gormDb.Where("id = ?", permissionId).First(&permission)
	response := new(pkg.Response)
	if permission.ID == 0 {
		response.Status = -1
		response.Message = "权限不存在"
		return ctx.JSON(response)
	}

	_, err := permission.Delete()

	if err != nil {
		response.Status = -1
		response.Message = err.Error()

		return ctx.JSON(response)
	}

	response.Message = "删除成功"

	return ctx.JSON(response)
}

// 权限详情
func Detail(ctx *fiber.Ctx) error {
	permissionId := ctx.Params("id", "")
	gormDb := pkg.GetConfig().GormDb
	permission := new(models.Permission)

	gormDb.Where("id = ?", permissionId).Preload(clause.Associations).First(&permission)
	response := new(pkg.Response)
	if permission.ID == 0 {
		response.Status = -1
		response.Message = "权限不存在"
		return ctx.JSON(response)
	}

	return ctx.JSON(permission)
}

// 权限列表
func List(ctx *fiber.Ctx) error {
	page, _ := strconv.Atoi(ctx.Query("page", "0"))
	pageSize, _ := strconv.Atoi(ctx.Query("size", "1"))
	// 防止页码溢出
	if page < 1 {
		page = 1
	}
	// 计算偏移
	offset := (page - 1) * pageSize

	permissions := make([]models.Permission, 0)

	gormDb := pkg.GetConfig().GormDb
	gormDb.Limit(pageSize).Offset(offset).Order("id DESC").Find(&permissions)
	var total int64
	gormDb.Model(&models.Permission{}).Count(&total)

	response := new(pkg.Response)
	response.Data = fiber.Map{
		"total": total,
		"data":  permissions,
	}

	return ctx.JSON(response)
}
