package user

import (
	"github.com/gofiber/fiber/v2"
	"go-fiber-demo/pkg"
	userService "go-fiber-demo/services/user"
)

// 登录
func Register(ctx *fiber.Ctx) error {
	registerUser, err := userService.Register(ctx.FormValue("username"), ctx.FormValue("password"))
	response := pkg.Response{}
	if err == nil {
		response.Status = 0
		response.Data = registerUser.ID
		response.Message = "注册成功"
	} else {
		response.Status = -1
		response.Message = err.Error()
	}

	return ctx.JSON(response)
}
