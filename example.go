package main

import (
	"github.com/crazyhl/gopermission"
	"github.com/gofiber/fiber/v2"
	"go-fiber-demo/pkg"
	"go-fiber-demo/pkg/bootstrap"
	"go-fiber-demo/route"
)

func init() {

}

func main() {
	db := bootstrap.InitDb(pkg.GetConfig().DbConfig)
	gopermission.RegisterWithDb(db, func(modelName string, paramValue string, condition string, user map[string]interface{}) bool {
		return true
	})
	if !fiber.IsChild() {
		// 初始化 db
		bootstrap.AutoMergeModel(db)
		// 注册权限相关的方法
		gopermission.AutoMigrate(db)
	}

	// 注册路由或者其他在启动服务之前的方法
	pkg.AddStartServeFunc(func() {
		// 注册路由
		route.RegisterTestRoute()
	})

	bootstrap.NewCliApp()
	bootstrap.StartCommand()
}
