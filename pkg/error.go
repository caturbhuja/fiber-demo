package pkg

type ValidateError struct {
	Message string
}

func (validateError ValidateError) Error() string {
	return validateError.Message
}

type LoginError struct {
	Message string
}

func (validateError LoginError) Error() string {
	return validateError.Message
}
