package utils

import (
	"github.com/gofiber/fiber/v2"
	"go-fiber-demo/pkg"
)

func GetHandleFunc(handlers ...fiber.Handler) []fiber.Handler {
	handlers = append(pkg.GetConfig().AppConfig.ClosedRequests, handlers...)
	return handlers
}

func Get(router fiber.Router, path string, handlers ...fiber.Handler) {
	router.Get(path, GetHandleFunc(handlers...)...)
}

func GetWithoutClosedHandler(router fiber.Router, path string, handlers ...fiber.Handler) {
	router.Get(path, handlers...)
}

func Post(router fiber.Router, path string, handlers ...fiber.Handler) {
	router.Post(path, GetHandleFunc(handlers...)...)
}

func PostWithoutClosedHandler(router fiber.Router, path string, handlers ...fiber.Handler) {
	router.Post(path, handlers...)
}

func Put(router fiber.Router, path string, handlers ...fiber.Handler) {
	router.Put(path, GetHandleFunc(handlers...)...)
}

func PutWithoutClosedHandler(router fiber.Router, path string, handlers ...fiber.Handler) {
	router.Put(path, handlers...)
}

func Delete(router fiber.Router, path string, handlers ...fiber.Handler) {
	router.Delete(path, GetHandleFunc(handlers...)...)
}

func DeleteWithoutClosedHandler(router fiber.Router, path string, handlers ...fiber.Handler) {
	router.Delete(path, handlers...)
}

func Patch(router fiber.Router, path string, handlers ...fiber.Handler) {
	router.Patch(path, GetHandleFunc(handlers...)...)
}

func PatchWithoutClosedHandler(router fiber.Router, path string, handlers ...fiber.Handler) {
	router.Patch(path, handlers...)
}

func AddClosedRequest(handlers ...fiber.Handler) {
	pkg.GetConfig().AppConfig.ClosedRequests = append(pkg.GetConfig().AppConfig.ClosedRequests, handlers...)
}
