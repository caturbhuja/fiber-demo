package jwt

import (
	"github.com/form3tech-oss/jwt-go"
	"go-fiber-demo/pkg"
	"time"
)

func Encoding(values map[string]interface{}) (string, error) {
	// 生成 token
	token := jwt.New(jwt.SigningMethodHS256)
	claims := token.Claims.(jwt.MapClaims)

	for key, value := range values {
		claims[key] = value
	}

	userConfig := pkg.GetConfig()

	claims["ext"] = time.Now().Add(time.Hour * time.Duration(userConfig.JwtConfig.Ext)).Unix()

	return token.SignedString([]byte(userConfig.JwtConfig.TokenSecret))
}
