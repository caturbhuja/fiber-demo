package validator

import (
	"github.com/go-playground/validator/v10"
	"github.com/go-playground/validator/v10/translations/zh"
	"go-fiber-demo/pkg"
	"go-fiber-demo/pkg/utils/trans"
)

// 校验用户
func Validate(model interface{}) []error {
	var validateErrors []error
	validate := validator.New()
	err := validate.Struct(model)
	ts := trans.ZhTranslator()
	zh.RegisterDefaultTranslations(validate, ts)
	if err != nil {
		for _, err := range err.(validator.ValidationErrors) {
			validateErrors = append(validateErrors, pkg.ValidateError{
				Message: err.Translate(ts),
			})
		}
	}
	return validateErrors
}
