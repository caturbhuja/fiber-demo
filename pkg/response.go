package pkg

// 自定义的一个通用相应状态
//type MultiErrorTipResponse struct {
//	Status int         // 状态值
//	Data   interface{} // 成功状态下的数据字段
//	Messages []string    // 失败状态下的错误数据，兼容多个错误返回
//}

// 自定义的一个通用相应状态
type Response struct {
	Status  int         // 状态值
	Data    interface{} // 成功状态下的数据字段
	Message string      // 失败状态下的错误数据
}
