package bootstrap

import (
	"github.com/gofiber/fiber/v2"
	"go-fiber-demo/pkg"
	"time"
)

func NewApp(beforeStartServeFunc pkg.BeforeStartServeFunc) *fiber.App {
	// 单例
	app := pkg.GetConfig().App
	if app == nil {
		app := fiber.New(fiber.Config{
			ReadTimeout:  10 * time.Second,
			WriteTimeout: 20 * time.Second,
			Prefork:      true,
		})
		pkg.GetConfig().App = app
	}

	if beforeStartServeFunc != nil {
		beforeStartServeFunc()
	}

	return app
}

func StartServe() error {
	host := pkg.GetConfig().ServerConfig.Host
	port := pkg.GetConfig().ServerConfig.Port
	return pkg.GetConfig().App.Listen(host + ":" + port)
}
