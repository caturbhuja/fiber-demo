package models

import (
	"github.com/crazyhl/gopermission/config"
	"github.com/crazyhl/gopermission/models"
	"go-fiber-demo/pkg"
	"gorm.io/gorm/clause"
)

// 用户 设计的很松散，用户既可以跟角色绑定也可以跟权限绑定，最后获取的时候有一个返回全部权限的方法
type User struct {
	Base
	Username    string              `gorm:"type:char(20) NOT NULL;default: '';uniqueIndex" validate:"required,email"`
	Password    string              `gorm:"type:char(128) NOT NULL;default: '';"  validate:"required,min=6,max=32"`
	Rules       []models.Rule       `gorm:"many2many:user_rule;"`       // 用户角色关联
	Permissions []models.Permission `gorm:"many2many:user_permission;"` // 用户权限关联
}

// 返回所有的权限
func (u *User) FullPermissions() []models.Permission {
	fullPermissions := u.Permissions
	db := pkg.GetConfig().GormDb
	// 如果没有获取到权限就去数据库获取一下
	if len(fullPermissions) == 0 {
		db.Model(u).Association("Permissions").Find(&fullPermissions)
	}
	// 如果角色为空就去查询一次
	rules := u.Rules
	if len(rules) == 0 {
		db.Model(u).Preload(clause.Associations).Association("Rules").Find(&rules)
	}
	for _, rule := range rules {
		fullPermissions = append(fullPermissions, rule.Permissions...)
	}

	return fullPermissions
}

// 替换角色
func (u *User) ReplaceRules(rules []models.Rule) error {
	db := pkg.GetConfig().GormDb
	return db.Model(&u).Association("Rules").Replace(rules)
}

// 替换角色 通过 ids 数组
func (u *User) ReplaceRulesByIds(ids []int) error {
	db := config.GetConfig().GormDb
	rules := make([]models.Rule, 0)
	db.Where("id in ?", ids).Find(&rules)
	return u.ReplaceRules(rules)
}

// 替换权限
func (u *User) ReplacePermissions(permissions []models.Permission) error {
	db := config.GetConfig().GormDb
	return db.Model(&u).Association("Permissions").Replace(permissions)
}

// 替换权限 通过 ids 数组
func (u *User) ReplacePermissionsByIds(ids []int) error {
	db := config.GetConfig().GormDb
	permissions := make([]models.Permission, 0)
	db.Where("id in ?", ids).Find(&permissions)
	return u.ReplacePermissions(permissions)
}

// 清空角色
func (u *User) ClearRules() error {
	db := config.GetConfig().GormDb
	return db.Model(&u).Association("Rules").Clear()
}

// 清空权限
func (u *User) ClearPermissions() error {
	db := config.GetConfig().GormDb
	return db.Model(&u).Association("Permissions").Clear()
}
