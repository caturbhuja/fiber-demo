## 介绍

这是一个基于 `golang` 的 api 服务示例程序，采用的主要服务

* fibber http 服务
* jwt 前端验证
* gorm ORM 框架
* gopermission 权限验证

## 计划实现的部分

- [x] 用户登录注册
- [x] jwt 实现接口需要登录判定
- [x] 支持 command
- [x] 采用自行实现的权限判定策略 [gopermission](https://github.com/crazyhl/gopermission)
- [ ] 异步任务
- 待补充

## 使用方式
1. 创建超级管理员，执行下面的命令
```shell
go run example.go init add --username="username" --password="password"
```
此条命令会创建一个超级管理员账号，并把 `uid` 写入到配置文件中，本项目的超级管理员是采用配置判定的，其他的角色权限则是由数据库配置判定的，超级管理员具备一切权限。

2. 启动服务器
```shell
go run example.go serve
```
此条命令会启动 http 服务器提供服务。


## 实现这个的目的
第一次用 `golang` 做东西，准备按照自己的习惯实现一个基础的结构，当做后续的脚手架，或者用这个都实现以后，当做一个基础的微服务。

当做微服务的倾向更强，毕竟还可以学习一下 `rpc` 相关的东西，慢慢做着看