module go-fiber-demo

go 1.15

require (
	github.com/andybalholm/brotli v1.0.1 // indirect
	github.com/cosmtrek/air v1.21.2 // indirect
	github.com/cpuguy83/go-md2man/v2 v2.0.0 // indirect
	github.com/crazyhl/gopermission v1.0.18
	github.com/creack/pty v1.1.11 // indirect
	github.com/fatih/color v1.10.0 // indirect
	github.com/fatih/structs v1.1.0 // indirect
	github.com/form3tech-oss/jwt-go v3.2.2+incompatible
	github.com/go-playground/locales v0.13.0
	github.com/go-playground/universal-translator v0.17.0
	github.com/go-playground/validator/v10 v10.4.1
	github.com/gofiber/fiber/v2 v2.5.0
	github.com/gofiber/jwt/v2 v2.1.0
	github.com/imdario/mergo v0.3.11 // indirect
	github.com/klauspost/compress v1.11.12 // indirect
	github.com/leodido/go-urn v1.2.1 // indirect
	github.com/pelletier/go-toml v1.8.1 // indirect
	github.com/russross/blackfriday/v2 v2.1.0 // indirect
	github.com/urfave/cli/v2 v2.3.0
	github.com/valyala/fasthttp v1.22.0 // indirect
	github.com/valyala/tcplisten v1.0.0 // indirect
	golang.org/x/crypto v0.0.0-20210220033148-5ea612d1eb83
	golang.org/x/sys v0.0.0-20210309074719-68d13333faf2 // indirect
	gopkg.in/yaml.v2 v2.4.0
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b
	gorm.io/driver/mysql v1.0.4
	gorm.io/gorm v1.21.3
)
