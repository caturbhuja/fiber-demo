package main_test

import (
	"go-fiber-demo/pkg/utils"
	"go-fiber-demo/pkg/utils/encoder/aes"
	"testing"
)

func TestRandString(t *testing.T) {
	str := utils.RandString(16)
	t.Log(str)
}

func TestAESEncrypt(t *testing.T) {
	str := aes.CBCEncode("test123")

	t.Log(str)
}

func TestAESDecrpyt(t *testing.T) {
	str := aes.CBCDecode("JjRLt6UFk9ptvISpeuO4h3WbjLtP5B06eLmxwi/6brw=")
	t.Log(str)
}
